:- set_prolog_flag(unknown,fail). % closed world assumption

:- op(450,fy,rd). %legge e non consuma
:- op(450,fy,in). %legge e consuma
:- op(450,fy,out). %scrive

out(T) :- 
	\+ T, 
	assert(T), 
	serveWaitQueue(T).
	
out_d(T) :- 
	assert(T), 
	serveWaitQueue(T). %creates a tuple even if already present
	
rd(T) :- 
	T.

rd(T) :- 
	\+ T, 
	fail.
	
rd_susp(T) :- 
	T.
	
rd_susp(T) :- 
	\+ T, 
	log(T), 
	assert(tuple_s(rd,T,$this)), % $me stands for the GameObject that called this method
	fail. 

%retrieves the tuple T, destructive
in(T) :- 
	T, 
	retract(T).

%retrieves the tuple T, destructive
in_susp(T) :- 
	T, 
	retract(T).
	
in_susp(T) :- 
	assert(tuple_s(in,T,$this)),
	fail.


serveWaitQueue(T) :- 
	loopUntilIN(T,[],L), 
	/* log(L), */ 
	serveAgents(L).

loopUntilIN(T,Acc,L) :- 
	\+ member(tuple_s(in,T,_),Acc), 
	tuple_s(M,T,V), 
	retract(tuple_s(M,T,V)), 
	append(Acc,[tuple_s(M,T,V)],Y), 
	loopUntilIN(T,Y,L).
	
loopUntilIN(_,L,L).

serveAgents([]).

serveAgents([H|T]) :- 
	processAgent(H), 
	serveAgents(T).

processAgent(tuple_s(rd,_,A)) :- 
	call_method(A, 'awakeAgent', _).
	
processAgent(tuple_s(in,T,A)) :- 
	retract(T), 
	call_method(A, 'awakeAgent', _).

%implementazione uniform rd e in

u_rd(R,T) :- 
	atomic(T), 
	atom_string(X,T), 
	findall(F,call(X,F), L), 
	random_member(R,L). 
	
u_rd(R,T) :- 
	findall(R, T, L), 
	log(L), 
	random_member(R,L).
	
u_in(R,T) :- 
	atomic(T), 
	atom_string(X,T), 
	findall(F,call(X,F), L), 
	log(L),  
	retract(T), 
	random_member(R,L). 
	
u_in(R,T) :- 
	log(T), 
	findall(R, T, L), 
	log(L), 
	retract(T), 
	random_member(R,L).