%===========CONVERSION===========%
ref_to_agent(Ref,Ag) :-
    Ref \= null,
    (
        is_class(Ref,$'UnityLogic.Agent'),
        Ag is Ref
        ;
        Ag is Ref.getcomponent($'UnityLogic.Agent') /*fa sempre questo e non quello sopra*/
    ),
    Ag \= null.
    
ref_to_artifact(Ref,Art) :-
    Ref \= null,
    (
        is_class(Ref,$'UnityLogic.Artifact'),        
        Art is Ref
        ;
        Art is Ref.getcomponent($'UnityLogic.Artifact') /*fa sempre questo e non quello sopra*/
    ),
    Art \= null.

%===========ACTION CHECKING===========%	
find_actions(A, List, Result) :- /*List è di appoggio per la ricorsione, in Result ci metto il risultato*/
	(A = [act Action|T],
		(Action = (M,_) ; Action = M, M \= (_,_,_), M \= (_,_)), /*Cosi non considero le act che hanno gia un riferimento, andranno nel terzo caso*/
		get_ref(Ref),
		ref_to_agent(Ref,Ag),
		call_method(Ref,'IsMethodAvailable'(Ag,M),Ret),
		(
			Ret = true,
			append(List, [act Action], Conc),
			find_actions(T,Conc, Result)
			;
			Ret = false,
			ref_agent_to_learn(RefAgentToLearn),
			(
				Action = (M,R),
				append(List, [act (@RefAgentToLearn,M,R)], Conc)
				;
				append(List, [act (@RefAgentToLearn,M)], Conc)
			),
			find_actions(T,Conc, Result)
		)
	);
	(A = [cr Coroutine|T],
		Coroutine \= (_,_), /*non considero le cr che hanno gia un riferimento*/
		get_ref(Ref),
		ref_to_agent(Ref,Ag),
		call_method(Ref,'IsMethodAvailable'(Ag,Coroutine),Ret),
		(
			(Ret = true,
			append(List, [cr Coroutine], Conc),
			find_actions(T,Conc, Result))
			;
			(Ret = false,
			ref_agent_to_learn(RefAgentToLearn),
			append(List, [cr (@RefAgentToLearn,Coroutine)], Conc),
			find_actions(T,Conc, Result))
		)
	);
	(A = [Something|T],
		append(List, [Something], Conc),
		find_actions(T,Conc, Result))
	;
	(A = [],
		Result = List,
		retract(ref_agent_to_learn(RefAgentToLearn))
	).
    
%============ARTIFACT=============%
learn_artifact_belief(Ref,B) :-
    ref_to_artifact(Ref,Art),
    call_method(Art,'LearnBelief'(B),Ret),
    Ret \= false,
    add_belief(B).
    
check_artifact_belief(Ref,B) :-
    ref_to_artifact(Ref,Art),
    call_method(Art,'LearnBelief'(B),Ret),
    Ret \= false.
    
add_artifact_belief(Ref,B) :-
    ref_to_artifact(Ref,Art),
    call_method(Art,'AddBelief'(B),Ret),
    Ret \= false.

del_artifact_belief(Ref,B) :-
    ref_to_artifact(Ref,Art),
    call_method(Art,'DelBelief'(B),Ret),
    Ret \= false.
    
activate_artifact(Ref,Action) :-
    ref_to_artifact(Ref,Art),
    call_method(Art,'Activate'(Action),Ret),
    Ret.
    
use_artifact(Ref,Action,Ret) :-
    ref_to_artifact(Ref,Art),
    call_method(Art,'Use'(Action),Ret),
    Ret \= false.
    
%============AGENT================%
learn_agent_belief(Ref,B) :-
    ref_to_agent(Ref,Ag),
    call_method(Ag,'LearnBelief'(B),Ret),
    Ret \= false,
    add_belief(B).
    
check_agent_belief(Ref,B) :-
    ref_to_agent(Ref,Ag),
    call_method(Ag,'LearnBelief'(B),Ret),
    Ret \= false.
    
add_agent_desire(Ref,D) :-
    ref_to_agent(Ref,Ag),
    call_method(Ag,'AddDesire'(D),Ret),
    Ret \= false.
    
del_agent_desire(Ref,D) :-
    ref_to_agent(Ref,Ag),
    call_method(Ag,'DelDesire'(D),Ret),
    Ret \= false.
    
add_agent_belief(Ref,B) :-
    ref_to_agent(Ref,Ag),
    call_method(Ag,'AddBelief'(B),Ret),
    Ret \= false.
    
del_agent_belief(Ref,B) :-
    ref_to_agent(Ref,Ag),
    call_method(Ag,'DelBelief'(B),Ret),
    Ret \= false.
    
learn_agent_plan(Ref,Head,Raw) :-
    ref_to_agent(Ref,Ag),
    (Head = add _ ; Head = del _),
    call_method(Ag,'CheckPlan'(Head),Ret),
    Ret = Head && C => A,
	assert(ref_agent_to_learn(Ag)), /*Mi salvo il riferimento dell'agente su cui eventualmente dovro richiamare le azioni che non ho nel mio script*/
	find_actions(A, _, Result),
	NewRet = Head && C => Result,
    (Raw, !, assert(NewRet) ; convert_plan(Ag,A,Converted), assert(Head && C => Converted)). /*La roba dopo il ; non serve a niente*/