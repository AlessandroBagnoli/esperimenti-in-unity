:- consult("UnityLogic/KBs/UnityLogicAgent.prolog").
:- consult("UnityLogic/KBs/CommunicationLayer.prolog").

%==================ENTRY_POINT==============%

/* se c'è una coroutine attiva ne esegue uno step chiamando MoveNext sull'IEnumerator (la coroutine) */
go_on :-
    /active_task/task:Task,
    Task \= null,
    call_method(Task,'MoveNext'(),Ret), 
    (Ret = false, assert(/active_task/task:null) ; true).

/* se non c'è una coroutine attiva, contollo se c'è qualche continuazione da portare avanti e ne eseguo un passo */
go_on :-
    /active_task/cont:Cont,
	/current_desire/current:D,
	D \= null,
    Cont \= null,
    task_completed.

/* Eseguo il primo piano della lista dei piani */    
go_on :-
    /active_task/plans:Plans,
    Plans = [H|T],
	assert(/active_task/plans:T),
	H = [Precondition|Actions],
	(
		Precondition,
		set_active_task(Actions)
		;
		go_on
	).

/* Ho un desiderio e un piano associato a quel desiderio, raccolgo tutti i piani per quel desiderio (e ad ogni piano ci aggiungo in coda l'istruzione per togliere il desiderio) in una lista */
go_on :-
    desire D,
    add D && _ => _,
    findall(
        X,
		/*(add D && C => A, append([C],A,Temp), append(Temp,[del_desire(D)],X)), /*Poli lo aveva fatto cosi, richiamando sempre il goal deletion anche quando il goal era andato a buon fine, ora faccio retract del desiderio e basta */
        (add D && C => A, append([C],A,Temp), append(Temp,[retract(desire D)],X)),
        Plans
    ),
	assert(/current_desire/current:D),
    assert(/active_task/plans:Plans).

/* Per stoppare l'esecuzione di piani alternativi */
stop :-
    assert(/active_task/plans:null).

%==================BELIEF==================%
add_belief(B) :-
    assert(belief B).
 
del_belief(B) :-
    retract(belief B).
    
%==================DESIRE=================%
add_desire(D) :-
    desire D 
    ; 
    assert(desire D).
    
del_desire(D) :-
    retract(desire D),
    (
        del D && _ => _, !, /*se hai un goal deletion definito lo esegui, altrimenti stampa un log per avvisare il programmatore che non esiste e se lo deve scrivere*/
		findall(
        X,
		(del D && C => A, append([C],A,X)),
        Plans
		),
		assert(/active_task/plans:Plans)
        ; 
        log("No goal deletion available for desire: ", D, "!")
    ).
    
%==================PLAN==================%
check_plan(Event,Full) :-
    Event && C => A,
    Full = Event && C => A.