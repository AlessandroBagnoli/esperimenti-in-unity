:- consult("UnityLogic/KBs/UnityLogicAgentAPI.prolog").
:- consult("UnityLogic/KBs/LindaLibrary.prolog").

desire work.

add work && true => [
	add_desire(rescue)
].

add rescue && in(warning(injured)) => [
	log("Rescuer(PROLOG SIDE): in(warning(injured)) on my KB successful, im going to rescue the injured person"),
	cr goTo($'ToRescue')
	/*add_desire(verify)*/
].

/*add verify && in(warning(injured)) => [
	act printfromagent("Tupla trovata e rimossa dalla mia KB!"),
	add_desire(verifyagain)
].

add verify && (\+ in(warning(injured))) => [
	act printfromagent("Tupla non trovata nella mia KB, rimango in attesa!"),
	add_desire(verifyagain)
].

add verifyagain && true => [
	add_desire(verify)
].*/