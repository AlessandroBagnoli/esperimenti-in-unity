﻿using UnityLogic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using LindaCoordination;
using Linda;

public class Rescuer : Agent {

    private NavMeshAgent nav;

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    private readonly string warningTuple = "warning(injured)";

    void Start () {
        Init(kbPath, kbName);
        nav = GetComponent<NavMeshAgent>();
	}

    private void OnTriggerEnter(Collider coll)
    {
        if (LindaCoordinationUtilities.RetrieveMessageFromSituatedObjectTriggered(this.warningTuple, coll).Equals(ReturnTypeKB.True))
        {
            print("Rescuer: Injured person detected, adding tuple to my kb");
            LindaLibrary.Linda_OUT(this.warningTuple, base.myKB);
        }
    }

    public IEnumerator GoTo(GameObject target)
    {
        nav.enabled = true;
        nav.isStopped = false;

        nav.SetDestination(target.transform.position);

        while (!nav.enabled || nav.pathPending)
        {
            yield return null;
        }

        while (!nav.enabled || nav.remainingDistance > 1.5f)
        {
            yield return null;
        }

        nav.isStopped = true;
        nav.enabled = false;
    }
	
}
