﻿using LindaCoordination;
using System;
using System.Collections;
using System.Timers;
using UnityEngine;
using UnityEngine.AI;
using UnityLogic;

public class Person : Agent {

    private NavMeshAgent nav;

    public string kbPath = "KBs/PrologFile";
    public string kbName = "KbName";

    private readonly string warningTuple = "warning(injured)";

    void Start () {
        Init(kbPath, kbName);
        nav = GetComponent<NavMeshAgent>();
    }
	
	public void ReportInjury()
    {
        print("Person: I'm injured, creating region...");
        Region region = new Region("Zone", transform.position, new Vector3(35f, 10f, 20f), PrimitiveType.Cube, true, Quaternion.identity);
        LindaCoordinationUtilities.SendMessageToRegion(this.warningTuple, region);
    }

    public IEnumerator DoStuff()
    {
        int secondsToWait = new System.Random().Next(5, 10);
        Timer timer = new Timer(1000);
        timer.Start();
        timer.Elapsed += (object sender, ElapsedEventArgs e) => secondsToWait--;
        print("Person: Doing stuff for random time...");
        while (secondsToWait >= 0)
        {
            yield return null;
        }
    }
}
